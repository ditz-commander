#
# Author : Ben Lau <benlau@visionware.com.hk>
# License : New BSD License
#

import vte
import sys
import os
import gtk
import gobject
import yaml
from MainWindow import MainWindow
from gobject import GObject

class Commander(GObject):
	history = []
		
	__gsignals__ = {
		# "new-request" signal is emitted when user request to create a new instance of Commander
		'new-request' : (
				gobject.SIGNAL_RUN_FIRST,
					gobject.TYPE_NONE,()
				),
		# "quit-request" signal is emitted when user request to quit
		'quit-request' : (
				gobject.SIGNAL_RUN_FIRST,
					gobject.TYPE_NONE,()
				),				
	}
	
	def __init__(self):
		GObject.__init__(self)
		
		self.window = MainWindow()
		"""Instance of MainWindow"""
		
		self.vte = self.window.vte
		""" Instance of VTE"""

		self.issue_id = ""
		""" Current displaying issue """
		
		self.path = ""
		""" Current path without trailing '/' """
		
		self.repo_path = ""
		""" Path of ditz repository data file (e.g. self.path+"/bugs")  """
	
		window = self.window
		
		window.OpenButton.connect('clicked' , self.prompt_folder_selection)		
		
		window.AddButton.connect('clicked' , lambda button : self.run_ditz_cmd("add") )
		window.EditButton.connect('clicked' , lambda button : self.run_ditz_cmd("edit", self.window.issue_id) )
		window.CommentButton.connect('clicked' , lambda button : self.run_ditz_cmd("comment",self.window.issue_id) )
		window.AssignButton.connect('clicked' , lambda button : self.run_ditz_cmd("assign",self.window.issue_id) )
		window.StartButton.connect('clicked' , lambda button : self.run_ditz_cmd("start",self.window.issue_id) )
		window.CloseButton.connect('clicked' , lambda button : self.run_ditz_cmd("close",self.window.issue_id) )
		
		window.RawEditButton.connect('clicked' , lambda button : self.edit_issue_file(self.window.issue_id) )
		
		window.HtmlButton.connect('clicked' , lambda button : self.run_html_cmd() )
		
		#TODO - Commander should not bind the "clicked" event from window.*Button directory. It is the duty of _handle_do_signal()
		
		window.NewButton.connect('clicked' , lambda button : self.emit('new-request') )
		
		window.connect('do',lambda window , cmd : self._handle_do_signal(cmd) )
		window.connect('open-folder',lambda window , folder : self.open(folder) )
		window.connect('quit', lambda window : self.emit('quit-request') )
		
	def quit(self):
		self.window.destroy()
			
	def open(self,path):
		"""
		Open a ditz issue repository	
		"""
		self.path = path.rstrip('/')
		self.gohome()
		
		self.detect_repo()
                self.detect_config()
		
		# Index page may not be existed, so it is not always equal to window.homepage
                self.index_page = '%s/index.html' % self.config_html_dest
		
		self.html_created = os.path.exists(self.index_page)
		if self.html_created:
			self.window.homepage = "file://" + self.index_page
		else:
			self.window.homepage = "file://" + self.path
		self.window.browser.load_url(self.window.homepage)
		
		# Load history
		HISTORY_FILE = os.getenv("HOME")	+ "/.ditz-commander.history"
		self.history =[]
		try:
			file = open(HISTORY_FILE)
			for i in file:
				self.history.append(i.strip('\n'))
			file.close()
			
			if self.path in self.history:
				self.history.remove(self.path)
				
			self.history.insert(0,self.path)
			self.history = self.history[0:9]
		except IOError:
			self.history = [self.path]
			
		#Save history file
		try:
			file = open(HISTORY_FILE,"wt")
			#file.write(self.history)
			for i in self.history:
				file.write(i + "\n")
			file.close()
		except:
			print("Error writing history file %s\n" % HISTORY_FILE)
		
		self.window.set_open_file_history(self.history)	
			
		return
	
	# Prompt open Folder Selection Dialog		
	def prompt_folder_selection(self,button):
		dialog = gtk.FileChooserDialog(title = "Open Folder" , 
			action=gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
			buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                              gtk.STOCK_OK, gtk.RESPONSE_OK))
		response = dialog.run()	
		
		if (response == gtk.RESPONSE_OK):
			self.open(dialog.get_filename())
		dialog.destroy()
		return
		
	def gohome(self):
		"""
		Go back to home directory of the repository
		"""
		self.vte.feed_child("cd '" + self.path + "' \n")

	def show(self):
		"""
			show window
		"""
		self.window.show()

	def run_html_cmd(self):
		self.run_ditz_cmd("html", self.config_html_dest)
		gobject.timeout_add(2000,self.reload_browser)
		
	def reload_browser(self):
		if self.html_created:
			self.window.browser.reload(0)
		else:
			self.html_created = os.path.exists(self.index_page)
			if self.html_created:
				self.window.homepage = "file://" + self.index_page
				self.window.browser.load_url(self.window.homepage)
		return False

	#Exec a ditz command and grab focus
	def run_ditz_cmd(self,cmd,*args):
		ditz_cmd = "ditz " + cmd
		for i in args:
			ditz_cmd += " "
			ditz_cmd += i
		ditz_cmd += "\n"
		self.gohome()
		self.vte.feed_child(ditz_cmd)
		self.vte.grab_focus()

	# Open editor to edit issue file directly
	def edit_issue_file(self,issue_id):
		editor = os.getenv("EDITOR")
		if editor == None:
			editor = "vim"
		cmd = editor  + " bugs/issue-%s.yaml\n" % issue_id
		self.gohome()
		self.vte.feed_child(cmd)
		self.vte.grab_focus()
		
	def get_config(self, filename):
            '''Parse config file
            
            Finds config file relative to self.path with given name and parses
            it as YAML.
            '''
            config_filename = '%s/%s' % (self.path, filename)
            
            if not os.path.exists(config_filename):
                return None

            config_file = open(config_filename, 'rt')
            config = config_file.read().split('\n')
            config_file.close()

            # TODO: properly parse ditz YAML tags
            if len(config) > 0 and config[0] >= 3 and config[0][:3] == '---':
                config[0] = '---' # bypass ditz tag declaration
                                  # (in the case of .ditz-config)

            return yaml.load('\n'.join(config))

        def detect_config(self):
            '''Get configuration of ditz-commander'''
            config = self.get_config('.ditz-commander-config')
            if config is None:
                dialog = gtk.MessageDialog(
                    type=gtk.MESSAGE_INFO,
                    message_format='Ditz-commander configuration '+
                        '(".ditz-commander-config") not found! Assuming '+
                        'default configuration:\nhtml_dest: html\n\nYou can '+
                        'set you own configuration by creating file '+
                        '".ditz-commander-config" with the following lines:\n'+
                        'html_dest: <destination of generated HTML files>',
                    buttons=gtk.BUTTONS_OK)
                dialog.run()
                dialog.destroy()
                config_html_dest = 'html'
            else:
                config_html_dest = config['html_dest']

            if config_html_dest[0] == '/':
                self.config_html_dest = config_html_dest
            else:
                self.config_html_dest = '%s/%s' % (
                    self.path, config_html_dest)
        
        def detect_repo(self):
		"""
		Detect the path of ditz repository. If not found, it may prompt a dialog to correct it 
		"""
                self.repo_path = ''
		
                # Try to detect repository from ditz configuration
                config = self.get_config('.ditz-config')

                if not (config is None):
                    config_path = config['issue_dir']
                    
                    if config_path[0] == '/':
                        # absolute path
                        self.repo_path = config_path
                    else:
                        self.repo_path = '%s/%s' % (self.path, config_path)
                    
                    if not os.path.exists(self.repo_path):
                        self.repo_path = ''
		
		if self.repo_path == '':
			dialog = gtk.MessageDialog(type=gtk.MESSAGE_QUESTION,
				message_format = "Ditz repository is not created yet. Do you want to run 'ditz init'? ",
				buttons=gtk.BUTTONS_YES_NO)
			response = dialog.run()	
		
			if (response == gtk.RESPONSE_YES):
				self.run_ditz_cmd("init")
			dialog.destroy()

	# Handle the "do" signal emitted from MainWindow
	def _handle_do_signal(self,cmd):
		issue_cmd_list = [ "drop", "unassign" ,"stop",
			"set-component",
			"add-reference",
		]
		normal_cmd_list =["add-release" , "add-component" ]
		if cmd in issue_cmd_list:
			self.run_ditz_cmd(cmd,self.window.issue_id)
		elif cmd in normal_cmd_list:
			self.run_ditz_cmd(cmd)
		else:
			print ("Unknown command %s \n" % cmd)
		return
