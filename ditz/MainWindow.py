#
# Author : Ben Lau <benlau@visionware.com.hk>
# License : New BSD License
#
import gtk
import vte
import gtkmozembed
import sys
import os
import re
import gobject

class MainWindow(gtk.Window):
	
	# The button for issue related command
	issue_command_button_list = []
	
	__gsignals__ = {
		# "do" signal is emitted when a command should be executed. 
		# Closure : void (window , cmd )
		'do' : (
				gobject.SIGNAL_RUN_FIRST,
					gobject.TYPE_NONE,(gobject.TYPE_STRING,)
				),
		# "open" folder signal
		'open-folder' : (
				gobject.SIGNAL_RUN_FIRST,
					gobject.TYPE_NONE,(gobject.TYPE_STRING,)
			),
		# "quit" signal
		'quit' : (
				gobject.SIGNAL_RUN_FIRST,
					gobject.TYPE_NONE,()
			)
	}
	
	def __init__(self):
		#Current Issue ID
		self.issue_id = ""

		# Homepage of current project
		self.homepage = ""

		gtk.Window.__init__(self)		
		
		self.accel_group = gtk.AccelGroup()
		""" Accel Group """
		
		self.add_accel_group(self.accel_group)
		
		self._create_ui()
		# Signal binding
		self._signal_connect()

		return
				
	# Set sensitive for all button for issue related command			
	def set_issue_command_button_sensitive(self,status):
		for i in self.issue_command_button_list:
			i.set_sensitive(status)
	
	def set_open_file_history(self,history):
		RecentOpenMenu = gtk.Menu()
		for i in history:
			item = gtk.MenuItem(i)
			item.show()
			item.connect("activate",lambda menuitem,folder : self.emit('open-folder',folder) , i)
			RecentOpenMenu.append(item)
		self.OpenButton.set_menu(RecentOpenMenu)
		return
				
	def _create_ui(self):
		#Create UI
		
		width = 800
		height = 600
		if gtk.gdk.screen_width() < width:
			width = gtk.gdk.screen_width()
		if gtk.gdk.screen_height() * 7 / 10 < height:
			height = gtk.gdk.screen_height() * 7 / 10
		
		self.set_title("Ditz Commander")
		self.vbox = gtk.VBox()
		self.add(self.vbox)
		
		self._create_toolbar()
		
		self.vpaned = gtk.VPaned()
		self.vpaned.show()
		self.vbox.add(self.vpaned)
		
		self.browser = gtkmozembed.MozEmbed()
		self.browser.show()
		self.browser.set_size_request(width,height * 2 / 3)
		self.vpaned.add1(self.browser)
		
		self.vte = vte.Terminal ()
		self.vte.set_size_request(width,height / 3)
		self.vte.set_scroll_on_output(True)
		self.vte.show()
		self.vte.fork_command()
		self.vpaned.add2(self.vte)

		self._create_basic_command_list()
		self._create_extra_command_list()	
				
		self.vbox.show_all()
		self.ExtraButtonBox.hide()
		
		return
		
	def _create_toolbar(self):
		self.toolbar = gtk.Toolbar()
		self.vbox.pack_start(self.toolbar,expand=False,fill=True)
		
		self.NewButton = gtk.MenuToolButton(gtk.STOCK_NEW)
		self.NewButton.add_accelerator("clicked",self.accel_group,ord('N'),
			gtk.gdk.CONTROL_MASK,gtk.ACCEL_VISIBLE)
		self.toolbar.add(self.NewButton)
		
		self.OpenButton = gtk.MenuToolButton(gtk.STOCK_OPEN)
		self.OpenButton.add_accelerator("clicked",self.accel_group,ord('O'),
			gtk.gdk.CONTROL_MASK,gtk.ACCEL_VISIBLE)
		self.toolbar.add(self.OpenButton)
		
		separator = gtk.SeparatorToolItem()
		self.toolbar.add(separator)
		
		self.GoBackButton = gtk.ToolButton(gtk.STOCK_GO_BACK)
		self.GoBackButton.add_accelerator("clicked",self.accel_group,gtk.gdk.keyval_from_name("Left"),
			gtk.gdk.MOD1_MASK,gtk.ACCEL_VISIBLE)
		self.toolbar.add(self.GoBackButton)

		self.GoForwardButton = gtk.ToolButton(gtk.STOCK_GO_FORWARD)
		self.GoForwardButton.add_accelerator("clicked",self.accel_group,gtk.gdk.keyval_from_name("Right"),
			gtk.gdk.MOD1_MASK,gtk.ACCEL_VISIBLE)
		self.toolbar.add(self.GoForwardButton)
		
		self.ReloadButton = gtk.ToolButton(gtk.STOCK_REFRESH)
		self.ReloadButton.add_accelerator("clicked",self.accel_group,ord('R'),
			gtk.gdk.CONTROL_MASK,gtk.ACCEL_VISIBLE)
		self.toolbar.add(self.ReloadButton)
		
		self.HomeButton = gtk.ToolButton(gtk.STOCK_HOME)
		self.HomeButton.add_accelerator("clicked",self.accel_group,gtk.gdk.keyval_from_name("Home"),
			gtk.gdk.MOD1_MASK,gtk.ACCEL_VISIBLE)
		self.toolbar.add(self.HomeButton)
				
		separator = gtk.SeparatorToolItem()
		self.toolbar.add(separator)
		
		self.QuitButton = gtk.ToolButton(gtk.STOCK_QUIT)
		self.QuitButton.add_accelerator("clicked",self.accel_group,ord('Q'),
			gtk.gdk.CONTROL_MASK,gtk.ACCEL_VISIBLE)
		self.toolbar.add(self.QuitButton)
		return
	
	def _create_basic_command_list(self):
		# HBox to hold ButtonBox1 and ButtonBox2
		hbox = gtk.HBox()
		self.vbox.pack_end(hbox,expand=False,fill=True)
		
		LeftButtonBox = gtk.HBox()
		#LeftButtonBox.set_layout(gtk.BUTTONBOX_START)
		LeftButtonBox.set_homogeneous(False)
		hbox.pack_start(LeftButtonBox,expand=True)
		
		RightButtonBox = gtk.HButtonBox()
		RightButtonBox.set_layout(gtk.BUTTONBOX_END)
		hbox.add(RightButtonBox)

		self.MoreButton = gtk.ToggleButton(label="^")
		self.MoreButton.set_relief(gtk.RELIEF_NONE)
		LeftButtonBox.add(self.MoreButton)

		self.AddButton = gtk.Button(label="Add")
		self.AddButton.set_relief(gtk.RELIEF_NONE)
		LeftButtonBox.add(self.AddButton)
		
		self.EditButton = gtk.Button(label="Edit")
		self.EditButton.set_relief(gtk.RELIEF_NONE)
		self.EditButton.set_sensitive(False)
		self.issue_command_button_list.append(self.EditButton)
		LeftButtonBox.add(self.EditButton)

		self.CommentButton = gtk.Button(label="Comment")
		self.CommentButton.set_relief(gtk.RELIEF_NONE)
		self.CommentButton.set_sensitive(False)
		self.issue_command_button_list.append(self.CommentButton)
		LeftButtonBox.add(self.CommentButton)

		self.AssignButton = gtk.Button(label="Assign")
		self.AssignButton.set_relief(gtk.RELIEF_NONE)
		self.AssignButton.set_sensitive(False)
		self.issue_command_button_list.append(self.AssignButton)
		LeftButtonBox.add(self.AssignButton)

		self.UnassignButton = gtk.Button(label="Unassign")
		self.UnassignButton.set_relief(gtk.RELIEF_NONE)
		self.UnassignButton.set_sensitive(False)
		self.issue_command_button_list.append(self.UnassignButton)
		LeftButtonBox.add(self.UnassignButton)

		self.StartButton = gtk.Button(label="Start")
		self.StartButton.set_relief(gtk.RELIEF_NONE)
		self.StartButton.set_sensitive(False)
		self.issue_command_button_list.append(self.StartButton)
		LeftButtonBox.add(self.StartButton)
		
		self.StopButton = gtk.Button(label="Stop")
		self.StopButton.set_relief(gtk.RELIEF_NONE)
		self.StopButton.set_sensitive(False)
		self.issue_command_button_list.append(self.StopButton)
		LeftButtonBox.add(self.StopButton)
	
		self.CloseButton = gtk.Button(label="Close")
		self.CloseButton.set_relief(gtk.RELIEF_NONE)
		self.CloseButton.set_sensitive(False)
		self.issue_command_button_list.append(self.CloseButton)
		LeftButtonBox.add(self.CloseButton)

		self.HtmlButton = gtk.Button(label="HTML")
		self.HtmlButton.set_relief(gtk.RELIEF_NONE)
		RightButtonBox.add(self.HtmlButton)	
		
	def _create_extra_command_list(self):
		
		# Button Box holding extra / uncommon commands
		self.ExtraButtonBox = gtk.HButtonBox()
		self.ExtraButtonBox.set_homogeneous(False)
		self.ExtraButtonBox.set_layout(gtk.BUTTONBOX_START)

		button = self._add_command_button("Add Release","add-release",self.ExtraButtonBox)
		button = self._add_command_button("Add Component","add-component",self.ExtraButtonBox)
		
		button = self._add_command_button("Set Component","set-component",self.ExtraButtonBox)
		self.issue_command_button_list.append(button)

		button = self._add_command_button("Add Reference","add-reference",self.ExtraButtonBox)
		self.issue_command_button_list.append(button)
		
		# Raw Edit , Edit raw issue file.
		self.RawEditButton = gtk.Button(label = "Raw Edit")
		self.RawEditButton.set_relief(gtk.RELIEF_NONE)
		self.ExtraButtonBox.add(self.RawEditButton)
		self.issue_command_button_list.append(self.RawEditButton)

		# Drop an issue
		self.DropButton = gtk.Button(label = "Drop")
		self.DropButton.set_relief(gtk.RELIEF_NONE)
		self.ExtraButtonBox.add(self.DropButton)
		self.issue_command_button_list.append(self.DropButton)
		
		self.vbox.pack_end(self.ExtraButtonBox,expand=False,fill=True)
		
		return self.ExtraButtonBox	

	# Add button and connect signal
	def _add_command_button(self,text,cmd,container = None):
		button = gtk.Button(label = text)
		button.set_relief(gtk.RELIEF_NONE)
		if container:
			container.add(button)
		button.connect('clicked' , lambda button : self.emit('do',cmd) )
		return button
		
	# Signal connect	
	def _signal_connect(self):
		self.connect('delete-event', lambda window, event: self.emit('quit'))
		self.QuitButton.connect('clicked', lambda button: self.emit('quit') )
		self.vte.connect('child-exited',lambda vte: self.emit('quit'))
		
		self.browser.connect('location',lambda  mozembed : self._on_location_changed(self.browser.get_location())  )	
		self.browser.connect('title',lambda  mozembed : self._on_title_changed(self.browser.get_title())  )	
		
		self.GoBackButton.connect('clicked',lambda  button : self.browser.go_back())
		self.GoForwardButton.connect('clicked',lambda  button : self.browser.go_forward())
		self.MoreButton.connect('toggled', self._more_button_toggled_callback)

		self.DropButton.connect('clicked' , lambda button : self._prompt_confirm_drop_dialog() )
		
		self.UnassignButton.connect('clicked' , lambda button : self.emit('do',"unassign") )
		self.StopButton.connect('clicked' , lambda button : self.emit('do',"stop") )

		self.ReloadButton.connect('clicked' , lambda button : self.browser.reload(0) )
		self.HomeButton.connect('clicked' , lambda button : self.browser.load_url(self.homepage) )

	# Called when browser's location changed
	def _on_location_changed(self,uri):
		pattern = r'^.*issue-(?P<ID>[0-9a-zA-Z]{40}).html'
		m = re.search(pattern,uri)
		if m:
			self.issue_id = m.group('ID')
			self.set_issue_command_button_sensitive(True)
		else:
			self.issue_id = ""
			self.set_issue_command_button_sensitive(False)
		return
		
	# Called when browser's title changed
	def _on_title_changed(self,title):
		self.set_title("%s - Ditz Commander" % title)

	def _more_button_toggled_callback(self,button):
		if button.get_active():
			self.ExtraButtonBox.show()
		else:
			self.ExtraButtonBox.hide()
		return

	def _prompt_confirm_drop_dialog(self):
		dialog = gtk.MessageDialog(
			parent = self,
			type = gtk.MESSAGE_QUESTION,
			message_format = "Are you sure want to drop this issue?",
			buttons=gtk.BUTTONS_YES_NO)
		response = dialog.run()	
		
		if (response == gtk.RESPONSE_YES):
			self.emit("do","drop")
		dialog.destroy()

		return
