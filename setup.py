#!/usr/bin/env python

from distutils.core import setup

setup(name='Ditz Commander',
      version='1.0',
      description='GUI Frontend for Ditz',
      author='Ben Lau',
      author_email='benlau@visionware.com.hk',
      url='',
      packages=['ditz'],
      scripts=['ditz-commander']
     )
